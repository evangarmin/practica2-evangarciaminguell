﻿namespace VentanaVisual
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventanaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verLaAyudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soporteTécnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarActualizacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeGarlandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSoluciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónDeLaCuentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.irAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarYReemplazarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deshacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rehacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seleccionarTodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.listaDeErroresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeTareasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cuadroDeHerramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pantallaCompletaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tareaSiguienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tareaAnteriorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventanaPropiedadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton3 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.administrarConexionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proyectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repositorioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.proyectoOSoluciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carpetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.convertirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirDesdeElControlDelCódigoFuenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.nuevoProyectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proyectoExistenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlArchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAArchivoExistenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlTipoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.irAlMiembroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.búsquedaRápidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reemplazoRápidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alternarMarcadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.habilitarTodosLosMarcadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obtenerHerramientasYCaracterísticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extensionesYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conectarConLaBaseDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.conectarConElServidorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ejecutarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depurarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeReproducciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónDePruebasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarPerfilParaPruebasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventanasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.pruebasSeleccionadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasLasPruebasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebasConErroresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebasNoEjecutadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebasCorrectasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasLasPruebasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirArcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.todasLasPruebasToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.ejecutarPruebasDespuésDeCompilarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exploradorDePruebasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generadorDePerfilesDeRendimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ejecutarAnálisisDeCódigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enLaSoluciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarAnálisisDeCódigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paraLaSoluciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paraGarlandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calcularMétricasDeCódigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.ventanasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.paraLaSoluciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.paraLosProyectosSeleccionadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultadosDeMétricasDeCódigoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaVentanaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dividirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hacerFlotanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.acoplarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ocultarAutomáticamenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ocultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anclarPestañaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.gatoCantandoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscandoGatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.googleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoProyectoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.establecerPropiedadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarElementoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.añadirImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.editarToolStripMenuItem,
            this.verToolStripMenuItem,
            this.equipoToolStripMenuItem,
            this.herramientasToolStripMenuItem,
            this.pruebaToolStripMenuItem,
            this.analizarToolStripMenuItem,
            this.ventanaToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.abrirToolStripMenuItem,
            this.toolStripSeparator3,
            this.agregarToolStripMenuItem,
            this.toolStripSeparator4,
            this.cerrarToolStripMenuItem,
            this.cerrarSoluciónToolStripMenuItem,
            this.toolStripSeparator5,
            this.guardarToolStripMenuItem,
            this.guardarComoToolStripMenuItem,
            this.toolStripSeparator6,
            this.configuraciónDeLaCuentaToolStripMenuItem,
            this.toolStripSeparator7,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.irAToolStripMenuItem,
            this.buscarYReemplazarToolStripMenuItem,
            this.toolStripSeparator8,
            this.deshacerToolStripMenuItem,
            this.rehacerToolStripMenuItem,
            this.toolStripSeparator9,
            this.cortarToolStripMenuItem,
            this.copiarToolStripMenuItem,
            this.pegarToolStripMenuItem,
            this.eliminarToolStripMenuItem,
            this.toolStripSeparator10,
            this.seleccionarTodoToolStripMenuItem,
            this.toolStripSeparator11,
            this.marcadoresToolStripMenuItem});
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.editarToolStripMenuItem.Text = "Editar";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listaDeErroresToolStripMenuItem,
            this.notificacionesToolStripMenuItem,
            this.salidaToolStripMenuItem,
            this.listaDeTareasToolStripMenuItem,
            this.cuadroDeHerramientasToolStripMenuItem,
            this.toolStripSeparator12,
            this.pantallaCompletaToolStripMenuItem,
            this.toolStripSeparator13,
            this.tareaSiguienteToolStripMenuItem,
            this.tareaAnteriorToolStripMenuItem,
            this.toolStripSeparator14,
            this.ventanaPropiedadesToolStripMenuItem});
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.verToolStripMenuItem.Text = "Ver";
            // 
            // equipoToolStripMenuItem
            // 
            this.equipoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrarConexionesToolStripMenuItem});
            this.equipoToolStripMenuItem.Name = "equipoToolStripMenuItem";
            this.equipoToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.equipoToolStripMenuItem.Text = "Equipo";
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.obtenerHerramientasYCaracterísticasToolStripMenuItem,
            this.extensionesYToolStripMenuItem,
            this.toolStripSeparator21,
            this.conectarConLaBaseDeDatosToolStripMenuItem,
            this.conectarConElServidorToolStripMenuItem});
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.herramientasToolStripMenuItem.Text = "Herramientas";
            // 
            // pruebaToolStripMenuItem
            // 
            this.pruebaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejecutarToolStripMenuItem,
            this.depurarToolStripMenuItem,
            this.listaDeReproducciónToolStripMenuItem,
            this.configuraciónDePruebasToolStripMenuItem,
            this.generarPerfilParaPruebasToolStripMenuItem,
            this.toolStripSeparator22,
            this.ventanasToolStripMenuItem});
            this.pruebaToolStripMenuItem.Name = "pruebaToolStripMenuItem";
            this.pruebaToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.pruebaToolStripMenuItem.Text = "Prueba";
            // 
            // analizarToolStripMenuItem
            // 
            this.analizarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generadorDePerfilesDeRendimientoToolStripMenuItem,
            this.toolStripSeparator24,
            this.ejecutarAnálisisDeCódigoToolStripMenuItem,
            this.configurarAnálisisDeCódigoToolStripMenuItem,
            this.toolStripSeparator25,
            this.calcularMétricasDeCódigoToolStripMenuItem,
            this.toolStripSeparator26,
            this.ventanasToolStripMenuItem1});
            this.analizarToolStripMenuItem.Name = "analizarToolStripMenuItem";
            this.analizarToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.analizarToolStripMenuItem.Text = "Analizar";
            // 
            // ventanaToolStripMenuItem
            // 
            this.ventanaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaVentanaToolStripMenuItem,
            this.dividirToolStripMenuItem,
            this.toolStripSeparator27,
            this.hacerFlotanteToolStripMenuItem,
            this.acoplarToolStripMenuItem,
            this.ocultarAutomáticamenteToolStripMenuItem,
            this.ocultarToolStripMenuItem,
            this.toolStripSeparator28,
            this.anclarPestañaToolStripMenuItem});
            this.ventanaToolStripMenuItem.Name = "ventanaToolStripMenuItem";
            this.ventanaToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.ventanaToolStripMenuItem.Text = "Ventana";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verLaAyudaToolStripMenuItem,
            this.toolStripSeparator1,
            this.registrarProductoToolStripMenuItem,
            this.soporteTécnicoToolStripMenuItem,
            this.buscarActualizacionesToolStripMenuItem,
            this.toolStripSeparator2,
            this.acercaDeGarlandToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // verLaAyudaToolStripMenuItem
            // 
            this.verLaAyudaToolStripMenuItem.Name = "verLaAyudaToolStripMenuItem";
            this.verLaAyudaToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F1";
            this.verLaAyudaToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.verLaAyudaToolStripMenuItem.Text = "Ver la ayuda";
            // 
            // registrarProductoToolStripMenuItem
            // 
            this.registrarProductoToolStripMenuItem.Name = "registrarProductoToolStripMenuItem";
            this.registrarProductoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.registrarProductoToolStripMenuItem.Text = "Registrar producto";
            // 
            // soporteTécnicoToolStripMenuItem
            // 
            this.soporteTécnicoToolStripMenuItem.Name = "soporteTécnicoToolStripMenuItem";
            this.soporteTécnicoToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.soporteTécnicoToolStripMenuItem.Text = "Soporte técnico";
            // 
            // buscarActualizacionesToolStripMenuItem
            // 
            this.buscarActualizacionesToolStripMenuItem.Name = "buscarActualizacionesToolStripMenuItem";
            this.buscarActualizacionesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.buscarActualizacionesToolStripMenuItem.Text = "Buscar actualizaciones";
            // 
            // acercaDeGarlandToolStripMenuItem
            // 
            this.acercaDeGarlandToolStripMenuItem.Name = "acercaDeGarlandToolStripMenuItem";
            this.acercaDeGarlandToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.acercaDeGarlandToolStripMenuItem.Text = "Acerca de Garland";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(189, 6);
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proyectoToolStripMenuItem,
            this.repositorioToolStripMenuItem,
            this.archivoToolStripMenuItem1});
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.nuevoToolStripMenuItem.Text = "Nuevo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proyectoOSoluciónToolStripMenuItem,
            this.carpetaToolStripMenuItem,
            this.toolStripSeparator19,
            this.abrirDesdeElControlDelCódigoFuenteToolStripMenuItem,
            this.toolStripSeparator20,
            this.archivoToolStripMenuItem2,
            this.convertirToolStripMenuItem});
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            // 
            // agregarToolStripMenuItem
            // 
            this.agregarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoProyectoToolStripMenuItem,
            this.proyectoExistenteToolStripMenuItem});
            this.agregarToolStripMenuItem.Name = "agregarToolStripMenuItem";
            this.agregarToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.agregarToolStripMenuItem.Text = "Agregar";
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            // 
            // cerrarSoluciónToolStripMenuItem
            // 
            this.cerrarSoluciónToolStripMenuItem.Name = "cerrarSoluciónToolStripMenuItem";
            this.cerrarSoluciónToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.cerrarSoluciónToolStripMenuItem.Text = "Cerrar solución";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+S";
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.guardarComoToolStripMenuItem.Text = "Guardar como";
            // 
            // configuraciónDeLaCuentaToolStripMenuItem
            // 
            this.configuraciónDeLaCuentaToolStripMenuItem.Name = "configuraciónDeLaCuentaToolStripMenuItem";
            this.configuraciónDeLaCuentaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.configuraciónDeLaCuentaToolStripMenuItem.Text = "Configuración de la cuenta...";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.ShortcutKeyDisplayString = "Alt+F4";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(233, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(233, 6);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(233, 6);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(233, 6);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(233, 6);
            // 
            // irAToolStripMenuItem
            // 
            this.irAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.irAlArchivoToolStripMenuItem,
            this.irAArchivoExistenteToolStripMenuItem,
            this.irAlTipoToolStripMenuItem,
            this.irAlMiembroToolStripMenuItem});
            this.irAToolStripMenuItem.Name = "irAToolStripMenuItem";
            this.irAToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.irAToolStripMenuItem.Text = "Ir a";
            // 
            // buscarYReemplazarToolStripMenuItem
            // 
            this.buscarYReemplazarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.búsquedaRápidaToolStripMenuItem,
            this.reemplazoRápidoToolStripMenuItem});
            this.buscarYReemplazarToolStripMenuItem.Name = "buscarYReemplazarToolStripMenuItem";
            this.buscarYReemplazarToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.buscarYReemplazarToolStripMenuItem.Text = "Buscar y reemplazar";
            // 
            // deshacerToolStripMenuItem
            // 
            this.deshacerToolStripMenuItem.Name = "deshacerToolStripMenuItem";
            this.deshacerToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Z";
            this.deshacerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.deshacerToolStripMenuItem.Text = "Deshacer";
            // 
            // rehacerToolStripMenuItem
            // 
            this.rehacerToolStripMenuItem.Enabled = false;
            this.rehacerToolStripMenuItem.Name = "rehacerToolStripMenuItem";
            this.rehacerToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Y";
            this.rehacerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.rehacerToolStripMenuItem.Text = "Rehacer";
            // 
            // cortarToolStripMenuItem
            // 
            this.cortarToolStripMenuItem.Name = "cortarToolStripMenuItem";
            this.cortarToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cortarToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.cortarToolStripMenuItem.Text = "Cortar";
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.copiarToolStripMenuItem.Text = "Copiar";
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Enabled = false;
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+V";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.pegarToolStripMenuItem.Text = "Pegar";
            // 
            // eliminarToolStripMenuItem
            // 
            this.eliminarToolStripMenuItem.Name = "eliminarToolStripMenuItem";
            this.eliminarToolStripMenuItem.ShortcutKeyDisplayString = "Supr";
            this.eliminarToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.eliminarToolStripMenuItem.Text = "Eliminar";
            // 
            // seleccionarTodoToolStripMenuItem
            // 
            this.seleccionarTodoToolStripMenuItem.Enabled = false;
            this.seleccionarTodoToolStripMenuItem.Name = "seleccionarTodoToolStripMenuItem";
            this.seleccionarTodoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.seleccionarTodoToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.seleccionarTodoToolStripMenuItem.Text = "Seleccionar todo";
            // 
            // marcadoresToolStripMenuItem
            // 
            this.marcadoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alternarMarcadorToolStripMenuItem,
            this.habilitarTodosLosMarcadoresToolStripMenuItem});
            this.marcadoresToolStripMenuItem.Name = "marcadoresToolStripMenuItem";
            this.marcadoresToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.marcadoresToolStripMenuItem.Text = "Marcadores";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(201, 6);
            // 
            // listaDeErroresToolStripMenuItem
            // 
            this.listaDeErroresToolStripMenuItem.Name = "listaDeErroresToolStripMenuItem";
            this.listaDeErroresToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E";
            this.listaDeErroresToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.listaDeErroresToolStripMenuItem.Text = "Lista de errores";
            // 
            // notificacionesToolStripMenuItem
            // 
            this.notificacionesToolStripMenuItem.Name = "notificacionesToolStripMenuItem";
            this.notificacionesToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+W, N";
            this.notificacionesToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.notificacionesToolStripMenuItem.Text = "Notificaciones";
            // 
            // salidaToolStripMenuItem
            // 
            this.salidaToolStripMenuItem.Name = "salidaToolStripMenuItem";
            this.salidaToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+O";
            this.salidaToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.salidaToolStripMenuItem.Text = "Salida";
            // 
            // listaDeTareasToolStripMenuItem
            // 
            this.listaDeTareasToolStripMenuItem.Name = "listaDeTareasToolStripMenuItem";
            this.listaDeTareasToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+º, T";
            this.listaDeTareasToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.listaDeTareasToolStripMenuItem.Text = "Lista de tareas";
            // 
            // cuadroDeHerramientasToolStripMenuItem
            // 
            this.cuadroDeHerramientasToolStripMenuItem.Name = "cuadroDeHerramientasToolStripMenuItem";
            this.cuadroDeHerramientasToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Alt+X";
            this.cuadroDeHerramientasToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.cuadroDeHerramientasToolStripMenuItem.Text = "Cuadro de herramientas";
            // 
            // pantallaCompletaToolStripMenuItem
            // 
            this.pantallaCompletaToolStripMenuItem.Name = "pantallaCompletaToolStripMenuItem";
            this.pantallaCompletaToolStripMenuItem.ShortcutKeyDisplayString = "Mayús.+Alt+Entrar";
            this.pantallaCompletaToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.pantallaCompletaToolStripMenuItem.Text = "Pantalla completa";
            // 
            // tareaSiguienteToolStripMenuItem
            // 
            this.tareaSiguienteToolStripMenuItem.Name = "tareaSiguienteToolStripMenuItem";
            this.tareaSiguienteToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.tareaSiguienteToolStripMenuItem.Text = "Tarea siguiente";
            // 
            // tareaAnteriorToolStripMenuItem
            // 
            this.tareaAnteriorToolStripMenuItem.Name = "tareaAnteriorToolStripMenuItem";
            this.tareaAnteriorToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.tareaAnteriorToolStripMenuItem.Text = "Tarea anterior";
            // 
            // ventanaPropiedadesToolStripMenuItem
            // 
            this.ventanaPropiedadesToolStripMenuItem.Name = "ventanaPropiedadesToolStripMenuItem";
            this.ventanaPropiedadesToolStripMenuItem.ShortcutKeyDisplayString = "F4";
            this.ventanaPropiedadesToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.ventanaPropiedadesToolStripMenuItem.Text = "Ventana Propiedades";
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(273, 6);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(273, 6);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(273, 6);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1,
            this.toolStripButton1,
            this.toolStripSeparator15,
            this.toolStripDropDownButton1,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripButton3,
            this.toolStripSeparator16,
            this.toolStripDropDownButton2,
            this.toolStripSplitButton2,
            this.toolStripSeparator17,
            this.toolStripComboBox1,
            this.toolStripTextBox1,
            this.toolStripSplitButton3,
            this.toolStripLabel1,
            this.toolStripProgressBar1,
            this.toolStripSeparator18,
            this.toolStripButton5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Enabled = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Navegar hacia delante (Ctrl+Mayús.+-)";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(39, 22);
            this.toolStripLabel1.Text = "Iniciar";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gatoCantandoToolStripMenuItem,
            this.buscandoGatoToolStripMenuItem,
            this.googleToolStripMenuItem,
            this.mSNToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(32, 22);
            this.toolStripSplitButton1.Text = "toolStripSplitButton1";
            this.toolStripSplitButton1.ToolTipText = "Navegar hacia atrás (Ctrl+-)";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoProyectoToolStripMenuItem1});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ToolTipText = "Nuevo proyecto (Ctrl+Mayús.+N)";
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.toolStripComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "Debug",
            "Release",
            "Administrador de configuración..."});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            this.toolStripComboBox1.Text = "Debug";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.toolStripTextBox1.ForeColor = System.Drawing.Color.Red;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox1.Text = "Any CPU";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 22);
            this.toolStripProgressBar1.Value = 40;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Abrir archivo (Ctrl+O)";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "Guardar como (Ctrl+Mayús.+S)";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "toolStripButton4";
            this.toolStripButton4.ToolTipText = "Guardar (Ctrl+S)";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.establecerPropiedadToolStripMenuItem,
            this.eliminarElementoToolStripMenuItem,
            this.añadirImagenToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton2.Text = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.ToolTipText = "Deshacer (Ctrl+Z)";
            // 
            // toolStripSplitButton2
            // 
            this.toolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton2.Enabled = false;
            this.toolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton2.Image")));
            this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton2.Name = "toolStripSplitButton2";
            this.toolStripSplitButton2.Size = new System.Drawing.Size(32, 22);
            this.toolStripSplitButton2.Text = "toolStripSplitButton2";
            this.toolStripSplitButton2.ToolTipText = "Rehacer (Ctrl+Y)";
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSplitButton3
            // 
            this.toolStripSplitButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarToolStripMenuItem});
            this.toolStripSplitButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton3.Image")));
            this.toolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton3.Name = "toolStripSplitButton3";
            this.toolStripSplitButton3.Size = new System.Drawing.Size(32, 22);
            this.toolStripSplitButton3.Text = "toolStripSplitButton3";
            this.toolStripSplitButton3.ToolTipText = "Iniciar";
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.ToolTipText = "Buscar en archivos (Ctrl+Mayús.+F)";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 52);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(562, 259);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.richTextBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(554, 233);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Consola";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(554, 233);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Diseño";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.Info;
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.richTextBox1.Size = new System.Drawing.Size(548, 227);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBox1.Font = new System.Drawing.Font("Verdana Pro Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBox1.Location = new System.Drawing.Point(580, 62);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(208, 27);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "Ajustes";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // administrarConexionesToolStripMenuItem
            // 
            this.administrarConexionesToolStripMenuItem.Name = "administrarConexionesToolStripMenuItem";
            this.administrarConexionesToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.administrarConexionesToolStripMenuItem.Text = "Administrar conexiones...";
            // 
            // proyectoToolStripMenuItem
            // 
            this.proyectoToolStripMenuItem.Name = "proyectoToolStripMenuItem";
            this.proyectoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+N";
            this.proyectoToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.proyectoToolStripMenuItem.Text = "Proyecto";
            // 
            // repositorioToolStripMenuItem
            // 
            this.repositorioToolStripMenuItem.Name = "repositorioToolStripMenuItem";
            this.repositorioToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.repositorioToolStripMenuItem.Text = "Repositorio";
            // 
            // archivoToolStripMenuItem1
            // 
            this.archivoToolStripMenuItem1.Name = "archivoToolStripMenuItem1";
            this.archivoToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+N";
            this.archivoToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.archivoToolStripMenuItem1.Text = "Archivo";
            // 
            // proyectoOSoluciónToolStripMenuItem
            // 
            this.proyectoOSoluciónToolStripMenuItem.Name = "proyectoOSoluciónToolStripMenuItem";
            this.proyectoOSoluciónToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+O";
            this.proyectoOSoluciónToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.proyectoOSoluciónToolStripMenuItem.Text = "Proyecto o solución";
            // 
            // carpetaToolStripMenuItem
            // 
            this.carpetaToolStripMenuItem.Name = "carpetaToolStripMenuItem";
            this.carpetaToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+Alt+O";
            this.carpetaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.carpetaToolStripMenuItem.Text = "Carpeta";
            // 
            // archivoToolStripMenuItem2
            // 
            this.archivoToolStripMenuItem2.Name = "archivoToolStripMenuItem2";
            this.archivoToolStripMenuItem2.ShortcutKeyDisplayString = "Ctrl+O";
            this.archivoToolStripMenuItem2.Size = new System.Drawing.Size(283, 22);
            this.archivoToolStripMenuItem2.Text = "Archivo";
            // 
            // convertirToolStripMenuItem
            // 
            this.convertirToolStripMenuItem.Name = "convertirToolStripMenuItem";
            this.convertirToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.convertirToolStripMenuItem.Text = "Convertir...";
            // 
            // abrirDesdeElControlDelCódigoFuenteToolStripMenuItem
            // 
            this.abrirDesdeElControlDelCódigoFuenteToolStripMenuItem.Name = "abrirDesdeElControlDelCódigoFuenteToolStripMenuItem";
            this.abrirDesdeElControlDelCódigoFuenteToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.abrirDesdeElControlDelCódigoFuenteToolStripMenuItem.Text = "Abrir desde el control del código fuente";
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(280, 6);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(280, 6);
            // 
            // nuevoProyectoToolStripMenuItem
            // 
            this.nuevoProyectoToolStripMenuItem.Name = "nuevoProyectoToolStripMenuItem";
            this.nuevoProyectoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nuevoProyectoToolStripMenuItem.Text = "Nuevo proyecto";
            // 
            // proyectoExistenteToolStripMenuItem
            // 
            this.proyectoExistenteToolStripMenuItem.Name = "proyectoExistenteToolStripMenuItem";
            this.proyectoExistenteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.proyectoExistenteToolStripMenuItem.Text = "Proyecto existente";
            // 
            // irAlArchivoToolStripMenuItem
            // 
            this.irAlArchivoToolStripMenuItem.Name = "irAlArchivoToolStripMenuItem";
            this.irAlArchivoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+T";
            this.irAlArchivoToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.irAlArchivoToolStripMenuItem.Text = "Ir al archivo...";
            // 
            // irAArchivoExistenteToolStripMenuItem
            // 
            this.irAArchivoExistenteToolStripMenuItem.Name = "irAArchivoExistenteToolStripMenuItem";
            this.irAArchivoExistenteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+R";
            this.irAArchivoExistenteToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.irAArchivoExistenteToolStripMenuItem.Text = "Ir a archivo existente...";
            // 
            // irAlTipoToolStripMenuItem
            // 
            this.irAlTipoToolStripMenuItem.Name = "irAlTipoToolStripMenuItem";
            this.irAlTipoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+1";
            this.irAlTipoToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.irAlTipoToolStripMenuItem.Text = "Ir al tipo...";
            // 
            // irAlMiembroToolStripMenuItem
            // 
            this.irAlMiembroToolStripMenuItem.Name = "irAlMiembroToolStripMenuItem";
            this.irAlMiembroToolStripMenuItem.ShortcutKeyDisplayString = "Alt+º";
            this.irAlMiembroToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.irAlMiembroToolStripMenuItem.Text = "Ir al miembro...";
            // 
            // búsquedaRápidaToolStripMenuItem
            // 
            this.búsquedaRápidaToolStripMenuItem.Name = "búsquedaRápidaToolStripMenuItem";
            this.búsquedaRápidaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.búsquedaRápidaToolStripMenuItem.Text = "Búsqueda rápida";
            // 
            // reemplazoRápidoToolStripMenuItem
            // 
            this.reemplazoRápidoToolStripMenuItem.Name = "reemplazoRápidoToolStripMenuItem";
            this.reemplazoRápidoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.reemplazoRápidoToolStripMenuItem.Text = "Reemplazo rápido...";
            // 
            // alternarMarcadorToolStripMenuItem
            // 
            this.alternarMarcadorToolStripMenuItem.Name = "alternarMarcadorToolStripMenuItem";
            this.alternarMarcadorToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.alternarMarcadorToolStripMenuItem.Text = "Alternar marcador";
            // 
            // habilitarTodosLosMarcadoresToolStripMenuItem
            // 
            this.habilitarTodosLosMarcadoresToolStripMenuItem.Name = "habilitarTodosLosMarcadoresToolStripMenuItem";
            this.habilitarTodosLosMarcadoresToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.habilitarTodosLosMarcadoresToolStripMenuItem.Text = "Habilitar todos los marcadores...";
            // 
            // obtenerHerramientasYCaracterísticasToolStripMenuItem
            // 
            this.obtenerHerramientasYCaracterísticasToolStripMenuItem.Name = "obtenerHerramientasYCaracterísticasToolStripMenuItem";
            this.obtenerHerramientasYCaracterísticasToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.obtenerHerramientasYCaracterísticasToolStripMenuItem.Text = "Obtener herramientas y características...";
            // 
            // extensionesYToolStripMenuItem
            // 
            this.extensionesYToolStripMenuItem.Name = "extensionesYToolStripMenuItem";
            this.extensionesYToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.extensionesYToolStripMenuItem.Text = "Extensiones y actualizaciones...";
            // 
            // conectarConLaBaseDeDatosToolStripMenuItem
            // 
            this.conectarConLaBaseDeDatosToolStripMenuItem.Name = "conectarConLaBaseDeDatosToolStripMenuItem";
            this.conectarConLaBaseDeDatosToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.conectarConLaBaseDeDatosToolStripMenuItem.Text = "Conectar con la base de datos";
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(281, 6);
            // 
            // conectarConElServidorToolStripMenuItem
            // 
            this.conectarConElServidorToolStripMenuItem.Name = "conectarConElServidorToolStripMenuItem";
            this.conectarConElServidorToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.conectarConElServidorToolStripMenuItem.Text = "Conectar con el servidor";
            // 
            // ejecutarToolStripMenuItem
            // 
            this.ejecutarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pruebasSeleccionadasToolStripMenuItem,
            this.todasLasPruebasToolStripMenuItem,
            this.pruebasConErroresToolStripMenuItem,
            this.pruebasNoEjecutadasToolStripMenuItem,
            this.pruebasCorrectasToolStripMenuItem});
            this.ejecutarToolStripMenuItem.Name = "ejecutarToolStripMenuItem";
            this.ejecutarToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.ejecutarToolStripMenuItem.Text = "Ejecutar";
            // 
            // depurarToolStripMenuItem
            // 
            this.depurarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.todasLasPruebasToolStripMenuItem1});
            this.depurarToolStripMenuItem.Name = "depurarToolStripMenuItem";
            this.depurarToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.depurarToolStripMenuItem.Text = "Depurar";
            // 
            // listaDeReproducciónToolStripMenuItem
            // 
            this.listaDeReproducciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirArcToolStripMenuItem,
            this.toolStripSeparator23,
            this.todasLasPruebasToolStripMenuItem2});
            this.listaDeReproducciónToolStripMenuItem.Name = "listaDeReproducciónToolStripMenuItem";
            this.listaDeReproducciónToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.listaDeReproducciónToolStripMenuItem.Text = "Lista de reproducción";
            // 
            // configuraciónDePruebasToolStripMenuItem
            // 
            this.configuraciónDePruebasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ejecutarPruebasDespuésDeCompilarToolStripMenuItem});
            this.configuraciónDePruebasToolStripMenuItem.Name = "configuraciónDePruebasToolStripMenuItem";
            this.configuraciónDePruebasToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.configuraciónDePruebasToolStripMenuItem.Text = "Configuración de pruebas";
            // 
            // generarPerfilParaPruebasToolStripMenuItem
            // 
            this.generarPerfilParaPruebasToolStripMenuItem.Name = "generarPerfilParaPruebasToolStripMenuItem";
            this.generarPerfilParaPruebasToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.generarPerfilParaPruebasToolStripMenuItem.Text = "Generar perfil para pruebas";
            // 
            // ventanasToolStripMenuItem
            // 
            this.ventanasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exploradorDePruebasToolStripMenuItem,
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem});
            this.ventanasToolStripMenuItem.Name = "ventanasToolStripMenuItem";
            this.ventanasToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.ventanasToolStripMenuItem.Text = "Ventanas";
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(213, 6);
            // 
            // pruebasSeleccionadasToolStripMenuItem
            // 
            this.pruebasSeleccionadasToolStripMenuItem.Enabled = false;
            this.pruebasSeleccionadasToolStripMenuItem.Name = "pruebasSeleccionadasToolStripMenuItem";
            this.pruebasSeleccionadasToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.pruebasSeleccionadasToolStripMenuItem.Text = "Pruebas seleccionadas";
            // 
            // todasLasPruebasToolStripMenuItem
            // 
            this.todasLasPruebasToolStripMenuItem.Name = "todasLasPruebasToolStripMenuItem";
            this.todasLasPruebasToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+R, A";
            this.todasLasPruebasToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.todasLasPruebasToolStripMenuItem.Text = "Todas las pruebas";
            // 
            // pruebasConErroresToolStripMenuItem
            // 
            this.pruebasConErroresToolStripMenuItem.Enabled = false;
            this.pruebasConErroresToolStripMenuItem.Name = "pruebasConErroresToolStripMenuItem";
            this.pruebasConErroresToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.pruebasConErroresToolStripMenuItem.Text = "Pruebas con errores";
            // 
            // pruebasNoEjecutadasToolStripMenuItem
            // 
            this.pruebasNoEjecutadasToolStripMenuItem.Enabled = false;
            this.pruebasNoEjecutadasToolStripMenuItem.Name = "pruebasNoEjecutadasToolStripMenuItem";
            this.pruebasNoEjecutadasToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.pruebasNoEjecutadasToolStripMenuItem.Text = "Pruebas no ejecutadas";
            // 
            // pruebasCorrectasToolStripMenuItem
            // 
            this.pruebasCorrectasToolStripMenuItem.Enabled = false;
            this.pruebasCorrectasToolStripMenuItem.Name = "pruebasCorrectasToolStripMenuItem";
            this.pruebasCorrectasToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.pruebasCorrectasToolStripMenuItem.Text = "Pruebas correctas";
            // 
            // todasLasPruebasToolStripMenuItem1
            // 
            this.todasLasPruebasToolStripMenuItem1.Name = "todasLasPruebasToolStripMenuItem1";
            this.todasLasPruebasToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+R, Ctrl+A";
            this.todasLasPruebasToolStripMenuItem1.Size = new System.Drawing.Size(249, 22);
            this.todasLasPruebasToolStripMenuItem1.Text = "Todas las pruebas";
            // 
            // abrirArcToolStripMenuItem
            // 
            this.abrirArcToolStripMenuItem.Name = "abrirArcToolStripMenuItem";
            this.abrirArcToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.abrirArcToolStripMenuItem.Text = "Abrir archivo de lista de reproducción";
            // 
            // todasLasPruebasToolStripMenuItem2
            // 
            this.todasLasPruebasToolStripMenuItem2.Name = "todasLasPruebasToolStripMenuItem2";
            this.todasLasPruebasToolStripMenuItem2.Size = new System.Drawing.Size(272, 22);
            this.todasLasPruebasToolStripMenuItem2.Text = "Todas las pruebas";
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(269, 6);
            // 
            // ejecutarPruebasDespuésDeCompilarToolStripMenuItem
            // 
            this.ejecutarPruebasDespuésDeCompilarToolStripMenuItem.Name = "ejecutarPruebasDespuésDeCompilarToolStripMenuItem";
            this.ejecutarPruebasDespuésDeCompilarToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.ejecutarPruebasDespuésDeCompilarToolStripMenuItem.Text = "Ejecutar pruebas después de compilar";
            // 
            // exploradorDePruebasToolStripMenuItem
            // 
            this.exploradorDePruebasToolStripMenuItem.Name = "exploradorDePruebasToolStripMenuItem";
            this.exploradorDePruebasToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E, T";
            this.exploradorDePruebasToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.exploradorDePruebasToolStripMenuItem.Text = "Explorador de pruebas";
            // 
            // resultadosDeLaCoberturaDeCódigoToolStripMenuItem
            // 
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem.Name = "resultadosDeLaCoberturaDeCódigoToolStripMenuItem";
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E, R";
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.resultadosDeLaCoberturaDeCódigoToolStripMenuItem.Text = "Resultados de la cobertura de código";
            // 
            // generadorDePerfilesDeRendimientoToolStripMenuItem
            // 
            this.generadorDePerfilesDeRendimientoToolStripMenuItem.Name = "generadorDePerfilesDeRendimientoToolStripMenuItem";
            this.generadorDePerfilesDeRendimientoToolStripMenuItem.ShortcutKeyDisplayString = "Alt+F2";
            this.generadorDePerfilesDeRendimientoToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.generadorDePerfilesDeRendimientoToolStripMenuItem.Text = "Generador de perfiles de rendimiento";
            // 
            // ejecutarAnálisisDeCódigoToolStripMenuItem
            // 
            this.ejecutarAnálisisDeCódigoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enLaSoluciónToolStripMenuItem,
            this.ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem});
            this.ejecutarAnálisisDeCódigoToolStripMenuItem.Name = "ejecutarAnálisisDeCódigoToolStripMenuItem";
            this.ejecutarAnálisisDeCódigoToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.ejecutarAnálisisDeCódigoToolStripMenuItem.Text = "Ejecutar análisis de código";
            // 
            // enLaSoluciónToolStripMenuItem
            // 
            this.enLaSoluciónToolStripMenuItem.Name = "enLaSoluciónToolStripMenuItem";
            this.enLaSoluciónToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.enLaSoluciónToolStripMenuItem.Text = "En la solución";
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(309, 6);
            // 
            // ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem
            // 
            this.ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem.Name = "ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem";
            this.ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem.Size = new System.Drawing.Size(273, 22);
            this.ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem.Text = "Ejecutar análisis de código en Garland";
            // 
            // configurarAnálisisDeCódigoToolStripMenuItem
            // 
            this.configurarAnálisisDeCódigoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paraLaSoluciónToolStripMenuItem,
            this.paraGarlandToolStripMenuItem});
            this.configurarAnálisisDeCódigoToolStripMenuItem.Name = "configurarAnálisisDeCódigoToolStripMenuItem";
            this.configurarAnálisisDeCódigoToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.configurarAnálisisDeCódigoToolStripMenuItem.Text = "Configurar análisis de código";
            // 
            // paraLaSoluciónToolStripMenuItem
            // 
            this.paraLaSoluciónToolStripMenuItem.Name = "paraLaSoluciónToolStripMenuItem";
            this.paraLaSoluciónToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.paraLaSoluciónToolStripMenuItem.Text = "Para la solución";
            // 
            // paraGarlandToolStripMenuItem
            // 
            this.paraGarlandToolStripMenuItem.Name = "paraGarlandToolStripMenuItem";
            this.paraGarlandToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.paraGarlandToolStripMenuItem.Text = "Para Garland";
            // 
            // calcularMétricasDeCódigoToolStripMenuItem
            // 
            this.calcularMétricasDeCódigoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paraLaSoluciónToolStripMenuItem1,
            this.paraLosProyectosSeleccionadosToolStripMenuItem});
            this.calcularMétricasDeCódigoToolStripMenuItem.Name = "calcularMétricasDeCódigoToolStripMenuItem";
            this.calcularMétricasDeCódigoToolStripMenuItem.Size = new System.Drawing.Size(312, 22);
            this.calcularMétricasDeCódigoToolStripMenuItem.Text = "Calcular métricas de código";
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(309, 6);
            // 
            // ventanasToolStripMenuItem1
            // 
            this.ventanasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resultadosDeMétricasDeCódigoToolStripMenuItem});
            this.ventanasToolStripMenuItem1.Name = "ventanasToolStripMenuItem1";
            this.ventanasToolStripMenuItem1.Size = new System.Drawing.Size(312, 22);
            this.ventanasToolStripMenuItem1.Text = "Ventanas";
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(309, 6);
            // 
            // paraLaSoluciónToolStripMenuItem1
            // 
            this.paraLaSoluciónToolStripMenuItem1.Name = "paraLaSoluciónToolStripMenuItem1";
            this.paraLaSoluciónToolStripMenuItem1.Size = new System.Drawing.Size(247, 22);
            this.paraLaSoluciónToolStripMenuItem1.Text = "Para la solución";
            // 
            // paraLosProyectosSeleccionadosToolStripMenuItem
            // 
            this.paraLosProyectosSeleccionadosToolStripMenuItem.Name = "paraLosProyectosSeleccionadosToolStripMenuItem";
            this.paraLosProyectosSeleccionadosToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.paraLosProyectosSeleccionadosToolStripMenuItem.Text = "Para los proyectos seleccionados";
            // 
            // resultadosDeMétricasDeCódigoToolStripMenuItem
            // 
            this.resultadosDeMétricasDeCódigoToolStripMenuItem.Name = "resultadosDeMétricasDeCódigoToolStripMenuItem";
            this.resultadosDeMétricasDeCódigoToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.resultadosDeMétricasDeCódigoToolStripMenuItem.Text = "Resultados de métricas de código";
            // 
            // nuevaVentanaToolStripMenuItem
            // 
            this.nuevaVentanaToolStripMenuItem.Enabled = false;
            this.nuevaVentanaToolStripMenuItem.Name = "nuevaVentanaToolStripMenuItem";
            this.nuevaVentanaToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.nuevaVentanaToolStripMenuItem.Text = "Nueva ventana";
            // 
            // dividirToolStripMenuItem
            // 
            this.dividirToolStripMenuItem.Enabled = false;
            this.dividirToolStripMenuItem.Name = "dividirToolStripMenuItem";
            this.dividirToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.dividirToolStripMenuItem.Text = "Dividir";
            // 
            // hacerFlotanteToolStripMenuItem
            // 
            this.hacerFlotanteToolStripMenuItem.Name = "hacerFlotanteToolStripMenuItem";
            this.hacerFlotanteToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.hacerFlotanteToolStripMenuItem.Text = "Hacer flotante";
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(207, 6);
            // 
            // acoplarToolStripMenuItem
            // 
            this.acoplarToolStripMenuItem.Enabled = false;
            this.acoplarToolStripMenuItem.Name = "acoplarToolStripMenuItem";
            this.acoplarToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.acoplarToolStripMenuItem.Text = "Acoplar";
            // 
            // ocultarAutomáticamenteToolStripMenuItem
            // 
            this.ocultarAutomáticamenteToolStripMenuItem.Name = "ocultarAutomáticamenteToolStripMenuItem";
            this.ocultarAutomáticamenteToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.ocultarAutomáticamenteToolStripMenuItem.Text = "Ocultar automáticamente";
            // 
            // ocultarToolStripMenuItem
            // 
            this.ocultarToolStripMenuItem.Name = "ocultarToolStripMenuItem";
            this.ocultarToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.ocultarToolStripMenuItem.Text = "Ocultar";
            // 
            // anclarPestañaToolStripMenuItem
            // 
            this.anclarPestañaToolStripMenuItem.Enabled = false;
            this.anclarPestañaToolStripMenuItem.Name = "anclarPestañaToolStripMenuItem";
            this.anclarPestañaToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.anclarPestañaToolStripMenuItem.Text = "Anclar pestaña";
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(207, 6);
            // 
            // gatoCantandoToolStripMenuItem
            // 
            this.gatoCantandoToolStripMenuItem.Name = "gatoCantandoToolStripMenuItem";
            this.gatoCantandoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gatoCantandoToolStripMenuItem.Text = "Gato cantando";
            // 
            // buscandoGatoToolStripMenuItem
            // 
            this.buscandoGatoToolStripMenuItem.Name = "buscandoGatoToolStripMenuItem";
            this.buscandoGatoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.buscandoGatoToolStripMenuItem.Text = "Buscando gato...";
            // 
            // googleToolStripMenuItem
            // 
            this.googleToolStripMenuItem.Name = "googleToolStripMenuItem";
            this.googleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.googleToolStripMenuItem.Text = "Google";
            // 
            // mSNToolStripMenuItem
            // 
            this.mSNToolStripMenuItem.Name = "mSNToolStripMenuItem";
            this.mSNToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mSNToolStripMenuItem.Text = "MSN";
            // 
            // nuevoProyectoToolStripMenuItem1
            // 
            this.nuevoProyectoToolStripMenuItem1.Name = "nuevoProyectoToolStripMenuItem1";
            this.nuevoProyectoToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl+Mayús.+N";
            this.nuevoProyectoToolStripMenuItem1.Size = new System.Drawing.Size(257, 22);
            this.nuevoProyectoToolStripMenuItem1.Text = "Nuevo proyecto...";
            // 
            // establecerPropiedadToolStripMenuItem
            // 
            this.establecerPropiedadToolStripMenuItem.Name = "establecerPropiedadToolStripMenuItem";
            this.establecerPropiedadToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.establecerPropiedadToolStripMenuItem.Text = "Establecer propiedad";
            // 
            // eliminarElementoToolStripMenuItem
            // 
            this.eliminarElementoToolStripMenuItem.Name = "eliminarElementoToolStripMenuItem";
            this.eliminarElementoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.eliminarElementoToolStripMenuItem.Text = "Eliminar elemento";
            // 
            // añadirImagenToolStripMenuItem
            // 
            this.añadirImagenToolStripMenuItem.Name = "añadirImagenToolStripMenuItem";
            this.añadirImagenToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.añadirImagenToolStripMenuItem.Text = "Añadir imagen";
            // 
            // iniciarToolStripMenuItem
            // 
            this.iniciarToolStripMenuItem.Name = "iniciarToolStripMenuItem";
            this.iniciarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.iniciarToolStripMenuItem.Text = "Iniciar";
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.Green;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Madrid, España",
            "Londres, Reino Unido",
            "París, Francia",
            "Berlín, Alemania",
            "Roma, Italia",
            "Washington, Estados Unidos"});
            this.comboBox1.Location = new System.Drawing.Point(580, 106);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(208, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.Text = "Madrid, España";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(590, 139);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 5;
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.DimGray;
            this.trackBar1.Location = new System.Drawing.Point(52, 326);
            this.trackBar1.Maximum = 20;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(295, 45);
            this.trackBar1.TabIndex = 6;
            this.trackBar1.Value = 17;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 29);
            this.button1.TabIndex = 7;
            this.button1.Text = "Zoom In";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(52, 412);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 29);
            this.button2.TabIndex = 8;
            this.button2.Text = "Zoom Out";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(157, 377);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 29);
            this.button3.TabIndex = 9;
            this.button3.Text = "Restablecer zoom";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(157, 409);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(115, 29);
            this.button4.TabIndex = 10;
            this.button4.Text = "Opciones...";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(314, 384);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(181, 19);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "Permitir tildes en las vocales";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(314, 416);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(181, 19);
            this.checkBox2.TabIndex = 12;
            this.checkBox2.Text = "Permitir letras en mayúscula";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(606, 313);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(106, 19);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Fecha en línea";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(606, 338);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(131, 19);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Fecha sin conexión";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.domainUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.domainUpDown1.ForeColor = System.Drawing.Color.Red;
            this.domainUpDown1.Items.Add("JavaScript");
            this.domainUpDown1.Items.Add("HTML");
            this.domainUpDown1.Items.Add("CSS");
            this.domainUpDown1.Items.Add("C++");
            this.domainUpDown1.Items.Add("C#");
            this.domainUpDown1.Items.Add("C");
            this.domainUpDown1.Location = new System.Drawing.Point(559, 393);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.domainUpDown1.Size = new System.Drawing.Size(192, 24);
            this.domainUpDown1.TabIndex = 15;
            this.domainUpDown1.Text = "JavaScript";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.domainUpDown1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Garland";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem agregarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSoluciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem configuraciónDeLaCuentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarYReemplazarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem deshacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rehacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem seleccionarTodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem marcadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeErroresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeTareasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cuadroDeHerramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem pantallaCompletaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem tareaSiguienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tareaAnteriorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem ventanaPropiedadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventanaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verLaAyudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem registrarProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem soporteTécnicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarActualizacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem acercaDeGarlandToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem proyectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repositorioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem proyectoOSoluciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carpetaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem abrirDesdeElControlDelCódigoFuenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem convertirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoProyectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proyectoExistenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlArchivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAArchivoExistenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlTipoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem irAlMiembroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem búsquedaRápidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reemplazoRápidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alternarMarcadorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem habilitarTodosLosMarcadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrarConexionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obtenerHerramientasYCaracterísticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extensionesYToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripMenuItem conectarConLaBaseDeDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conectarConElServidorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejecutarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebasSeleccionadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todasLasPruebasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebasConErroresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebasNoEjecutadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebasCorrectasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depurarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem todasLasPruebasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listaDeReproducciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirArcToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripMenuItem todasLasPruebasToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem configuraciónDePruebasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejecutarPruebasDespuésDeCompilarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarPerfilParaPruebasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripMenuItem ventanasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exploradorDePruebasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultadosDeLaCoberturaDeCódigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generadorDePerfilesDeRendimientoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem ejecutarAnálisisDeCódigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enLaSoluciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ejecutarAnálisisDeCódigoEnGarlandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurarAnálisisDeCódigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paraLaSoluciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paraGarlandToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripMenuItem calcularMétricasDeCódigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paraLaSoluciónToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem paraLosProyectosSeleccionadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStripMenuItem ventanasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resultadosDeMétricasDeCódigoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaVentanaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dividirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripMenuItem hacerFlotanteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acoplarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ocultarAutomáticamenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ocultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripMenuItem anclarPestañaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gatoCantandoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscandoGatoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem googleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mSNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoProyectoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem establecerPropiedadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarElementoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem añadirImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
    }
}

