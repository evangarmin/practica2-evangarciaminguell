package ventanaeclipse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToolBar;
import java.awt.Button;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JTextPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import java.awt.TextArea;
import javax.swing.JToggleButton;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class WindowForm extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowForm frame = new WindowForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WindowForm() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\evant\\Downloads\\Colegio Montessori 1\u00BA DAW\\Marcas\\1\u00AA evaluaci\u00F3n\\Ejercicios de pr\u00E1ctica\\K35721000001001-00-500x500.jpg"));
		setTitle("SweetVictory");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 521, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmAbrirArchivo = new JMenuItem("Abrir archivo");
		mnArchivo.add(mntmAbrirArchivo);
		
		JMenu mnArchivosRecientes = new JMenu("Archivos recientes");
		mnArchivo.add(mnArchivosRecientes);
		
		JMenuItem mntmTrabajopracticatxt = new JMenuItem("trabajopractica2.txt");
		mnArchivosRecientes.add(mntmTrabajopracticatxt);
		
		JMenuItem mntmEjerciciosjar = new JMenuItem("ejercicios8.jar");
		mnArchivosRecientes.add(mntmEjerciciosjar);
		
		JMenuItem mntmPruebasworkspacejava = new JMenuItem("pruebasWorkspace.java");
		mnArchivosRecientes.add(mntmPruebasworkspacejava);
		
		JSeparator separator = new JSeparator();
		mnArchivo.add(separator);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como");
		mnArchivo.add(mntmGuardarComo);
		
		JSeparator separator_1 = new JSeparator();
		mnArchivo.add(separator_1);
		
		JMenuItem mntmImprimir = new JMenuItem("Imprimir...");
		mnArchivo.add(mntmImprimir);
		
		JSeparator separator_2 = new JSeparator();
		mnArchivo.add(separator_2);
		
		JMenuItem mntmImportar = new JMenuItem("Importar");
		mnArchivo.add(mntmImportar);
		
		JMenuItem mntmExportar = new JMenuItem("Exportar");
		mnArchivo.add(mntmExportar);
		
		JSeparator separator_3 = new JSeparator();
		mnArchivo.add(separator_3);
		
		JMenu mnSwitchWorkspace = new JMenu("Switch Workspace");
		mnArchivo.add(mnSwitchWorkspace);
		
		JMenuItem mntmCusersequipoedocumentosmisproyectos = new JMenuItem("C:\\Users\\equipoE\\Documentos\\MisProyectos");
		mnSwitchWorkspace.add(mntmCusersequipoedocumentosmisproyectos);
		
		JMenuItem mntmOtros = new JMenuItem("Otros...");
		mnSwitchWorkspace.add(mntmOtros);
		
		JMenuItem mntmReiniciar = new JMenuItem("Reiniciar");
		mnArchivo.add(mntmReiniciar);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnEdicin.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnEdicin.add(mntmRehacer);
		
		JSeparator separator_4 = new JSeparator();
		mnEdicin.add(separator_4);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnEdicin.add(mntmCortar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEdicin.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEdicin.add(mntmPegar);
		
		JMenuItem mntmEliminar = new JMenuItem("Eliminar");
		mnEdicin.add(mntmEliminar);
		
		JMenu mnNewMenu = new JMenu("Fuente");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAlternarComentario = new JMenuItem("Alternar comentario");
		mnNewMenu.add(mntmAlternarComentario);
		
		JMenuItem mntmAadirComentarioDe = new JMenuItem("A\u00F1adir comentario de bloque");
		mnNewMenu.add(mntmAadirComentarioDe);
		
		JMenuItem mntmEliminarComentarioDe = new JMenuItem("Eliminar comentario de bloque");
		mnNewMenu.add(mntmEliminarComentarioDe);
		
		JMenuItem mntmGenerarElementoDe = new JMenuItem("Generar elemento de comentario");
		mnNewMenu.add(mntmGenerarElementoDe);
		
		JSeparator separator_5 = new JSeparator();
		mnNewMenu.add(separator_5);
		
		JMenuItem mntmAadirImporte = new JMenuItem("A\u00F1adir importe");
		mnNewMenu.add(mntmAadirImporte);
		
		JMenuItem mntmOrganizarImportes = new JMenuItem("Organizar importes");
		mnNewMenu.add(mntmOrganizarImportes);
		
		JMenuItem mntmOrdenarMiembros = new JMenuItem("Ordenar miembros...");
		mnNewMenu.add(mntmOrdenarMiembros);
		
		JMenuItem mntmLimpiar = new JMenuItem("Limpiar...");
		mnNewMenu.add(mntmLimpiar);
		
		JMenu mnRefractor = new JMenu("Refractor");
		menuBar.add(mnRefractor);
		
		JMenuItem mntmRenombrar = new JMenuItem("Renombrar...");
		mnRefractor.add(mntmRenombrar);
		
		JMenuItem mntmMover = new JMenuItem("Mover...");
		mnRefractor.add(mntmMover);
		
		JSeparator separator_6 = new JSeparator();
		mnRefractor.add(separator_6);
		
		JMenuItem mntmMigrarArchivoJar = new JMenuItem("Migrar archivo JAR...");
		mnRefractor.add(mntmMigrarArchivoJar);
		
		JMenuItem mntmCrearScript = new JMenuItem("Crear script...");
		mnRefractor.add(mntmCrearScript);
		
		JMenuItem mntmAplicarScript = new JMenuItem("Aplicar script...");
		mnRefractor.add(mntmAplicarScript);
		
		JMenuItem mntmHistoria = new JMenuItem("Historial...");
		mnRefractor.add(mntmHistoria);
		
		JMenu mnNavegar = new JMenu("Navegar");
		menuBar.add(mnNavegar);
		
		JMenuItem mntmAbrirTarea = new JMenuItem("Abrir tarea");
		mnNavegar.add(mntmAbrirTarea);
		
		JMenuItem mntmActivarTarea = new JMenuItem("Activar tarea");
		mnNavegar.add(mntmActivarTarea);
		
		JMenuItem mntmDesactivarTarea = new JMenuItem("Desactivar tarea");
		mnNavegar.add(mntmDesactivarTarea);
		
		JSeparator separator_26 = new JSeparator();
		mnNavegar.add(separator_26);
		
		JMenuItem mntmSiguiente = new JMenuItem("Siguiente");
		mnNavegar.add(mntmSiguiente);
		
		JMenuItem mntmAnterior = new JMenuItem("Anterior");
		mnNavegar.add(mntmAnterior);
		
		JSeparator separator_27 = new JSeparator();
		mnNavegar.add(separator_27);
		
		JMenuItem mntmAtrs = new JMenuItem("Atr\u00E1s");
		mnNavegar.add(mntmAtrs);
		
		JMenuItem mntmAdelante = new JMenuItem("Adelante");
		mnNavegar.add(mntmAdelante);
		
		JMenu mnBuscar = new JMenu("Buscar");
		menuBar.add(mnBuscar);
		
		JMenuItem mntmBuscar_1 = new JMenuItem("Buscar...");
		mnBuscar.add(mntmBuscar_1);
		
		JMenuItem mntmArchivo = new JMenuItem("Archivo...");
		mnBuscar.add(mntmArchivo);
		
		JMenuItem mntmJava = new JMenuItem("Java...");
		mnBuscar.add(mntmJava);
		
		JSeparator separator_22 = new JSeparator();
		mnBuscar.add(separator_22);
		
		JMenu mnTexto = new JMenu("Texto...");
		mnBuscar.add(mnTexto);
		
		JMenuItem mntmWorkspace = new JMenuItem("Workspace");
		mnTexto.add(mntmWorkspace);
		
		JMenuItem mntmProyecto = new JMenuItem("Proyecto");
		mnTexto.add(mntmProyecto);
		
		JMenuItem mntmArchivo_1 = new JMenuItem("Archivo");
		mnTexto.add(mntmArchivo_1);
		
		JMenuItem mntmConjuntoDeTrabajo = new JMenuItem("Conjunto de trabajo...");
		mnTexto.add(mntmConjuntoDeTrabajo);
		
		JSeparator separator_23 = new JSeparator();
		mnBuscar.add(separator_23);
		
		JMenu mnReferencias = new JMenu("Referencias");
		mnBuscar.add(mnReferencias);
		
		JMenuItem mntmWorkspace_1 = new JMenuItem("Workspace");
		mnReferencias.add(mntmWorkspace_1);
		
		JMenuItem mntmProyecto_2 = new JMenuItem("Proyecto");
		mnReferencias.add(mntmProyecto_2);
		
		JMenuItem mntmJerarqua_1 = new JMenuItem("Jerarqu\u00EDa");
		mnReferencias.add(mntmJerarqua_1);
		
		JMenuItem mntmConjuntoDeTrabajo_1 = new JMenuItem("Conjunto de trabajo...");
		mnReferencias.add(mntmConjuntoDeTrabajo_1);
		
		JMenu mnDeclaraciones = new JMenu("Declaraciones");
		mnBuscar.add(mnDeclaraciones);
		
		JMenuItem mntmWorkspace_2 = new JMenuItem("Workspace");
		mnDeclaraciones.add(mntmWorkspace_2);
		
		JMenuItem mntmProyecto_1 = new JMenuItem("Proyecto");
		mnDeclaraciones.add(mntmProyecto_1);
		
		JMenuItem mntmJerarqua = new JMenuItem("Jerarqu\u00EDa");
		mnDeclaraciones.add(mntmJerarqua);
		
		JMenuItem mntmConjuntoDeTrabajo_2 = new JMenuItem("Conjunto de trabajo...");
		mnDeclaraciones.add(mntmConjuntoDeTrabajo_2);
		
		JMenu mnImplementores = new JMenu("Implementores");
		mnBuscar.add(mnImplementores);
		
		JMenuItem mntmWorkspace_3 = new JMenuItem("Workspace");
		mnImplementores.add(mntmWorkspace_3);
		
		JMenuItem mntmProyecto_3 = new JMenuItem("Proyecto");
		mnImplementores.add(mntmProyecto_3);
		
		JMenuItem mntmJerarqua_2 = new JMenuItem("Conjunto de trabajo...");
		mnImplementores.add(mntmJerarqua_2);
		
		JMenu mnAccesoDeLectura = new JMenu("Acceso de lectura");
		mnBuscar.add(mnAccesoDeLectura);
		
		JMenuItem mntmWorkspace_4 = new JMenuItem("Workspace");
		mnAccesoDeLectura.add(mntmWorkspace_4);
		
		JMenuItem mntmProyecto_4 = new JMenuItem("Proyecto");
		mnAccesoDeLectura.add(mntmProyecto_4);
		
		JMenuItem mntmJerarqua_3 = new JMenuItem("Jerarqu\u00EDa");
		mnAccesoDeLectura.add(mntmJerarqua_3);
		
		JMenuItem mntmConjuntoDeTrabajo_3 = new JMenuItem("Conjunto de trabajo...");
		mnAccesoDeLectura.add(mntmConjuntoDeTrabajo_3);
		
		JMenu mnAccesoDeEscritura = new JMenu("Acceso de escritura");
		mnBuscar.add(mnAccesoDeEscritura);
		
		JMenuItem mntmWorkspace_5 = new JMenuItem("Workspace");
		mnAccesoDeEscritura.add(mntmWorkspace_5);
		
		JMenuItem mntmProyecto_5 = new JMenuItem("Proyecto");
		mnAccesoDeEscritura.add(mntmProyecto_5);
		
		JMenuItem mntmJerarqua_4 = new JMenuItem("Jerarqu\u00EDa");
		mnAccesoDeEscritura.add(mntmJerarqua_4);
		
		JMenuItem mntmConjuntoDeTrabajo_4 = new JMenuItem("Conjunto de trabajo...");
		mnAccesoDeEscritura.add(mntmConjuntoDeTrabajo_4);
		
		JSeparator separator_24 = new JSeparator();
		mnBuscar.add(separator_24);
		
		JMenu mnOcurrenciasEnEl = new JMenu("Ocurrencias en el archivo");
		mnBuscar.add(mnOcurrenciasEnEl);
		
		JMenuItem mntmIdentificar = new JMenuItem("Identificar");
		mnOcurrenciasEnEl.add(mntmIdentificar);
		
		JSeparator separator_25 = new JSeparator();
		mnBuscar.add(separator_25);
		
		JMenuItem mntmPruebasDeReferencia = new JMenuItem("Pruebas de referencia...");
		mnBuscar.add(mntmPruebasDeReferencia);
		
		JMenu mnProyecto = new JMenu("Proyecto");
		menuBar.add(mnProyecto);
		
		JMenuItem mntmAbrirProyecto = new JMenuItem("Abrir proyecto");
		mnProyecto.add(mntmAbrirProyecto);
		
		JMenuItem mntmCerrarProyecto = new JMenuItem("Cerrar proyecto");
		mnProyecto.add(mntmCerrarProyecto);
		
		JSeparator separator_19 = new JSeparator();
		mnProyecto.add(separator_19);
		
		JMenuItem mntmConstruirTodo = new JMenuItem("Construir todo");
		mnProyecto.add(mntmConstruirTodo);
		
		JMenuItem mntmConstruirProyecto = new JMenuItem("Construir proyecto");
		mnProyecto.add(mntmConstruirProyecto);
		
		JMenu mnConstruirConjuntoDe = new JMenu("Construir conjunto de trabajo");
		mnProyecto.add(mnConstruirConjuntoDe);
		
		JMenuItem mntmSeleccionarConjuntoDe = new JMenuItem("Seleccionar conjunto de trabajo");
		mnConstruirConjuntoDe.add(mntmSeleccionarConjuntoDe);
		
		JMenuItem mntmLimpiar_1 = new JMenuItem("Limpiar...");
		mnProyecto.add(mntmLimpiar_1);
		
		JRadioButtonMenuItem rdbtnmntmConstruirAutomticamente = new JRadioButtonMenuItem("Construir autom\u00E1ticamente");
		mnProyecto.add(rdbtnmntmConstruirAutomticamente);
		
		JSeparator separator_20 = new JSeparator();
		mnProyecto.add(separator_20);
		
		JMenuItem mntmGenerarJavadoc = new JMenuItem("Generar JavaDoc");
		mnProyecto.add(mntmGenerarJavadoc);
		
		JSeparator separator_21 = new JSeparator();
		mnProyecto.add(separator_21);
		
		JMenuItem mntmPropiedades = new JMenuItem("Propiedades...");
		mnProyecto.add(mntmPropiedades);
		
		JMenu mnEjecutar = new JMenu("Ejecutar");
		menuBar.add(mnEjecutar);
		
		JMenuItem mntmIniciar = new JMenuItem("Reanudar");
		mnEjecutar.add(mntmIniciar);
		
		JMenuItem mntmPausa = new JMenuItem("Pausa");
		mnEjecutar.add(mntmPausa);
		
		JMenuItem mntmStop = new JMenuItem("Stop");
		mnEjecutar.add(mntmStop);
		
		JMenuItem mntmDesconectar = new JMenuItem("Desconectar");
		mnEjecutar.add(mntmDesconectar);
		
		JMenuItem mntmPasoAdelante = new JMenuItem("Paso adelante");
		mnEjecutar.add(mntmPasoAdelante);
		
		JMenuItem mntmPasoAtrs = new JMenuItem("Paso atr\u00E1s");
		mnEjecutar.add(mntmPasoAtrs);
		
		JSeparator separator_18 = new JSeparator();
		mnEjecutar.add(separator_18);
		
		JMenuItem mntmEjecutar = new JMenuItem("Ejecutar");
		mnEjecutar.add(mntmEjecutar);
		
		JMenuItem mntmDebug = new JMenuItem("Debug");
		mnEjecutar.add(mntmDebug);
		
		JMenuItem mntmReportaje = new JMenuItem("Reportaje");
		mnEjecutar.add(mntmReportaje);
		
		JMenu mnVentana = new JMenu("Ventana");
		menuBar.add(mnVentana);
		
		JMenuItem mntmNuevaVentana = new JMenuItem("Nueva ventana");
		mnVentana.add(mntmNuevaVentana);
		
		JMenu mnEditor = new JMenu("Editor");
		mnVentana.add(mnEditor);
		
		JMenuItem mntmClonar = new JMenuItem("Clonar");
		mnEditor.add(mntmClonar);
		
		JMenuItem mntmAumentarZoom = new JMenuItem("Aumentar zoom...");
		mnEditor.add(mntmAumentarZoom);
		
		JMenuItem mntmDisminuirZoom = new JMenuItem("Disminuir zoom...");
		mnEditor.add(mntmDisminuirZoom);
		
		JMenu mnApariencia = new JMenu("Apariencia");
		mnVentana.add(mnApariencia);
		
		JCheckBoxMenuItem chckbxmntmOcultarBarraDe = new JCheckBoxMenuItem("Ocultar barra de herramientas");
		mnApariencia.add(chckbxmntmOcultarBarraDe);
		
		JCheckBoxMenuItem chckbxmntmOcultarBarraDe_1 = new JCheckBoxMenuItem("Ocultar barra de estado");
		mnApariencia.add(chckbxmntmOcultarBarraDe_1);
		
		JMenuItem mntmPantallaCompleta = new JMenuItem("Pantalla completa");
		mnApariencia.add(mntmPantallaCompleta);
		
		JSeparator separator_10 = new JSeparator();
		mnVentana.add(separator_10);
		
		JMenu mnMostrarVista = new JMenu("Mostrar vista");
		mnVentana.add(mnMostrarVista);
		
		JMenuItem mntmHormiga = new JMenuItem("Hormiga");
		mnMostrarVista.add(mntmHormiga);
		
		JMenuItem mntmConsola = new JMenuItem("Consola");
		mnMostrarVista.add(mntmConsola);
		
		JMenuItem mntmDeclaracin = new JMenuItem("Declaraci\u00F3n");
		mnMostrarVista.add(mntmDeclaracin);
		
		JMenuItem mntmNavegador = new JMenuItem("Navegador");
		mnMostrarVista.add(mntmNavegador);
		
		JMenuItem mntmProblemas = new JMenuItem("Problemas");
		mnMostrarVista.add(mntmProblemas);
		
		JMenuItem mntmTareas = new JMenuItem("Tareas");
		mnMostrarVista.add(mntmTareas);
		
		JSeparator separator_13 = new JSeparator();
		mnMostrarVista.add(separator_13);
		
		JMenuItem mntmOtros_1 = new JMenuItem("Otros...");
		mnMostrarVista.add(mntmOtros_1);
		
		JMenu mnPerspectiva = new JMenu("Perspectiva");
		mnVentana.add(mnPerspectiva);
		
		JMenuItem mntmAbrirPerspectiva = new JMenuItem("Abrir perspectiva");
		mnPerspectiva.add(mntmAbrirPerspectiva);
		
		JSeparator separator_14 = new JSeparator();
		mnPerspectiva.add(separator_14);
		
		JMenuItem mntmPersonalizarPerspectiva = new JMenuItem("Personalizar perspectiva");
		mnPerspectiva.add(mntmPersonalizarPerspectiva);
		
		JMenuItem mntmGuardarPerspectivaComo = new JMenuItem("Guardar perspectiva como...");
		mnPerspectiva.add(mntmGuardarPerspectivaComo);
		
		JMenuItem mntmReanudarPerspectiva = new JMenuItem("Reanudar perspectiva...");
		mnPerspectiva.add(mntmReanudarPerspectiva);
		
		JMenuItem mntmCerrarPerspectiva = new JMenuItem("Cerrar perspectiva");
		mnPerspectiva.add(mntmCerrarPerspectiva);
		
		JMenuItem mntmCerrarTodasLas = new JMenuItem("Cerrar todas las perspectivas");
		mnPerspectiva.add(mntmCerrarTodasLas);
		
		JSeparator separator_12 = new JSeparator();
		mnVentana.add(separator_12);
		
		JMenu mnNewMenu_1 = new JMenu("Navegaci\u00F3n");
		mnVentana.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Ense\u00F1ar men\u00FA de perspectivas");
		mnNewMenu_1.add(mntmNewMenuItem);
		
		JMenuItem mntmAccesoRpido = new JMenuItem("Acceso r\u00E1pido");
		mnNewMenu_1.add(mntmAccesoRpido);
		
		JSeparator separator_15 = new JSeparator();
		mnNewMenu_1.add(separator_15);
		
		JMenuItem mntmActivarEditor = new JMenuItem("Activar editor");
		mnNewMenu_1.add(mntmActivarEditor);
		
		JMenuItem mntmAnteriorEditor = new JMenuItem("Anterior editor");
		mnNewMenu_1.add(mntmAnteriorEditor);
		
		JMenuItem mntmSiguienteEditor = new JMenuItem("Siguiente editor");
		mnNewMenu_1.add(mntmSiguienteEditor);
		
		JSeparator separator_16 = new JSeparator();
		mnNewMenu_1.add(separator_16);
		
		JMenuItem mntmSiguienteVista = new JMenuItem("Siguiente vista");
		mnNewMenu_1.add(mntmSiguienteVista);
		
		JMenuItem mntmAnteriorVista = new JMenuItem("Anterior vista");
		mnNewMenu_1.add(mntmAnteriorVista);
		
		JSeparator separator_17 = new JSeparator();
		mnNewMenu_1.add(separator_17);
		
		JMenuItem mntmSiguientePerspectiva = new JMenuItem("Siguiente perspectiva");
		mnNewMenu_1.add(mntmSiguientePerspectiva);
		
		JMenuItem mntmAnteriorPerspectiva = new JMenuItem("Anterior perspectiva");
		mnNewMenu_1.add(mntmAnteriorPerspectiva);
		
		JSeparator separator_11 = new JSeparator();
		mnVentana.add(separator_11);
		
		JMenuItem mntmPreferencias = new JMenuItem("Preferencias");
		mnVentana.add(mntmPreferencias);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmBienvenido = new JMenuItem("Bienvenido");
		mnAyuda.add(mntmBienvenido);
		
		JSeparator separator_7 = new JSeparator();
		mnAyuda.add(separator_7);
		
		JMenuItem mntmAyudaDeContenidos = new JMenuItem("Ayuda de contenidos");
		mnAyuda.add(mntmAyudaDeContenidos);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnAyuda.add(mntmBuscar);
		
		JMenuItem mntmMostrarAyudaContextual = new JMenuItem("Mostrar ayuda contextual");
		mnAyuda.add(mntmMostrarAyudaContextual);
		
		JSeparator separator_8 = new JSeparator();
		mnAyuda.add(separator_8);
		
		JMenuItem mntmComprobarDescargas = new JMenuItem("Comprobar descargas");
		mnAyuda.add(mntmComprobarDescargas);
		
		JMenuItem mntmInstalarNuevoSoftware = new JMenuItem("Instalar nuevo software");
		mnAyuda.add(mntmInstalarNuevoSoftware);
		
		JMenuItem mntmWindowformStore = new JMenuItem("SweetVictory Store");
		mnAyuda.add(mntmWindowformStore);
		
		JSeparator separator_9 = new JSeparator();
		mnAyuda.add(separator_9);
		
		JMenuItem mntmAcercaDeWindowform = new JMenuItem("Acerca de SweetVictory");
		mnAyuda.add(mntmAcercaDeWindowform);
		
		JMenuItem mntmContribuir = new JMenuItem("Contribuir");
		mnAyuda.add(mntmContribuir);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		contentPane.add(toolBar, BorderLayout.NORTH);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Pesta\u00F1a principal", null, panel, null);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Don Quijote de La Mancha", null, panel_1, null);
		
		TextArea textArea = new TextArea();
		panel_1.add(textArea);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Nueva pesta\u00F1a", null, panel_2, null);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
	}
}
